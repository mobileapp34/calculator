import 'dart:ffi';
import 'dart:io';
import 'package:calculator/calculator.dart' as calculator;
import 'package:test/expect.dart';

class cal {
  double number1 = 0;
  double number2 = 0;
  double sum = 0;
  var sym;

  cal(double number1, double number2) {
    this.sym = sym;
    this.number1 = number1;
    this.number2 = number2;
    this.sum = 0;
  }

  void checkSymbol(var symbol) {
    switch (sym) {
      case "+":
        sum = sum + plus(number1, number2);
        break;
      case "-":
        sum = sum + minus(number1, number2);
        break;
      case "*":
        sum = sum + multiply(number1, number2);
        break;
      case "/":
        sum = sum + divide(number1, number2);
        break;
      default:
        return print("Error");
    }
  }

  double setSum() {
    return sum = 0;
  }

  double getSum() {
    return sum;
  }

  double plus(double number1, double number2) {
    return number1 + number2;
  }

  double minus(double number1, double number2) {
    return number1 - number2;
  }

  double multiply(double number1, double number2) {
    return number1 * number2;
  }

  double divide(double number1, double number2) {
    return number1 / number2;
  }

  double mod(double number1, double number2) {
    return number1%number2;
  }

  double percent(double number1, double number2) {
    return sum*(1/100);
  }
}

void main() {
  print("Please Input number1: ");
  double number1 = double.parse(stdin.readLineSync()!);
  bool quit = false;
  while (quit == false) {
    print("press the operator or quit): ");
    String? sym = stdin.readLineSync();
    if (sym == "exit") {
      break;
    }
    print("Please Input number2: ");
    double number2 = double.parse(stdin.readLineSync()!);
    cal Calculate = new cal(number1,number2);
    Calculate.checkSymbol(sym);
    print("Result = ${Calculate.getSum()}");
    number1 = Calculate.getSum();
  }
}
